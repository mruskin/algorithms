package hashTable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class firstRecurringCharacter {

    public static void main(String[] args) {
        System.out.println(findRecurring(new int[] { 1, 2, 4,7, 5 }));
        System.out.println(findRecurring2(new int[] { 2, 2, 4,7, 5, 1 }));
    }

    public static int findRecurring2(int[] arr) {
        Set<Integer> recurringMap = new HashSet<>();
        for (int i = 0; i < arr.length; i++) {
            if (recurringMap.contains(arr[i])) {
                return arr[i];
            } else {
                recurringMap.add(arr[i]);
            }
        }
        return -1;
    }

    // Bad solution
    public static int findRecurring(int[] arr) {
        Map<Integer, Integer> recurringMap = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            Integer freq = recurringMap.get(arr[i]);
            if (freq != null) {
                return arr[i];
            }
            recurringMap.put(arr[i], 0);
        }
        return -1;
    }
}
