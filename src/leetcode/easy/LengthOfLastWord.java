package leetcode.easy;

/**
 * https://leetcode.com/problems/length-of-last-word/
 *
 * Given a string s consisting of some words separated by some number of spaces, return the length of the last word in the string.
 *
 * A word is a maximal substring consisting of non-space characters only.
 */
public class LengthOfLastWord {

    public static void main(String[] args) {
        System.out.println(lengthOfLastWord("hello   noob  "));
    }

    public static int lengthOfLastWord(final String a)
    {
        int len = 0;
        /* String a is 'final'-- can not be modified
           So, create a copy and trim the spaces from
           both sides */
        String x = a.trim();

        for (int i = 0; i < x.length(); i++) {
            if (x.charAt(i) == ' ')
                len = 0;
            else
                len++;
        }
        return len;
    }

//    public static int lengthOfLastWord(String s) {
//        s = s.trim();
////        System.out.println(s);
//        String lastWord = s.substring(s.lastIndexOf(" ")+1);
////        System.out.println(lastWord);
//        return lastWord.length();
//    }


}
