package leetcode.easy;

/**
 * leetcode
 * https://leetcode.com/problems/verifying-an-alien-dictionary/
 *
 * In an alien language, surprisingly they also use english lowercase letters, but possibly in a different order.
 * The order of the alphabet is some permutation of lowercase letters.
 *
 * Given a sequence of words written in the alien language, and the order of the alphabet,
 * return true if and only if the given words are sorted lexicographicaly in this alien language.
 *
 * Example 1:
 *
 * Input: words = ["hello","leetcode"], order = "hlabcdefgijkmnopqrstuvwxyz"
 * Output: true
 * Explanation: As 'h' comes before 'l' in this language, then the sequence is sorted.
 * Example 2:
 *
 * Input: words = ["word","world","row"], order = "worldabcefghijkmnpqstuvxyz"
 * Output: false
 * Explanation: As 'd' comes after 'l' in this language, then words[0] > words[1], hence the sequence is unsorted.
 */
public class VerifyAlienDictionary {

    public static void main(String[] args) {

    }

    public static boolean isAlienSorted(String[] words, String order) {
        for (int i = 0; i < words.length;i++) {
            char[] word = words[i].toCharArray();
            char[] orderC = order.toCharArray();
            for (int j = 0; j < orderC.length; j++) {

            }
        }
        return true;
    }
}
