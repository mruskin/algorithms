package leetcode.easy;

/**
 * Write a function to find the longest common prefix string amongst an array of strings.
 *
 * If there is no common prefix, return an empty string "".
 *
 *
 *
 * Example 1:
 *
 * Input: strs = ["flower","flow","flight"]
 * Output: "fl"
 * Example 2:
 *
 * Input: strs = ["dog","racecar","car"]
 * Output: ""
 */
public class LongestCommonPrefix {

    public static void main(String[] args) {
        System.out.println(longestCommonPrefix(new String[]{"flower","flow","flight"}));
    }

    public static String longestCommonPrefix(String[] strs) {
        int longest = 0;
        int temp = 0;
        String pref = "";
        for (int i = 1; i < strs.length; i++) {

            for (int j = 0; j < strs[i].toCharArray().length; j++) {
                if (strs[i-1].charAt(j) == strs[i].charAt(j)) {
                    temp++;
                } else if (longest == 0) {
                    return "";
                }
            }
            if (longest > temp) {

            }
            longest = temp;
            temp = 0;

        }
        return pref;
    }
}
