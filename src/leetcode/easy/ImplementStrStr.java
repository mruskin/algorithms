package leetcode.easy;


/**
 * Implement strStr().
 *
 * Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.
 *
 * Clarification:
 *
 * What should we return when needle is an empty string? This is a great question to ask during an interview.
 *
 * For the purpose of this problem, we will return 0 when needle is an empty string. This is consistent to C's strstr() and Java's indexOf().
 */


// What should we return when needle is an empty string?
public class ImplementStrStr {

    public int strStr(String haystack, String needle) {
        // empty needle appears everywhere, first appears at 0 index
        if (needle.length() == 0) // todo: add this check in the end
            return 0;
        if (haystack.length() == 0)
            return -1;


        for (int i = 0; i < haystack.length(); i++) {
            // no enough places for needle after i
            if (i + needle.length() > haystack.length()) break; // todo: what if needle can't fit, let's break

            for (int j = 0; j < needle.length(); j++) {
                if (haystack.charAt(i+j) != needle.charAt(j))
                    break;
                if (j == needle.length()-1)
                    return i;
            }
        }

        return -1;
    }

//    public int strStr(String haystack, String needle) {
//        int res = 0;
//        char[] cA = needle.toCharArray();
//        char[] haystackArr = haystack.toCharArray();
//
//        if (needle == null || needle=="") {
//            return res;
//        }
//        for (int i = 0; i < haystackArr.length;i++) {
////            for (int j = 0; j < needle.length();j++) {
//
//                if (haystackArr[i] == cA[0]) {
//                    res = i;
//                    if (needle.length() > 1) {
//                        for (int j = 0; j < needle.length();j++) {
//                            if (haystackArr[]) {
//
//                            }
//                        }
//                    }
//                }
//
////            }
//        }
//        return res;
//    }
}
