package leetcode.easy;

import java.util.Arrays;
import java.util.Collections;

/**
 * Maximum Subarray
 * https://leetcode.com/problems/maximum-subarray/
 * Given an integer array nums, find the contiguous subarray (containing at least one number)
 * which has the largest sum and return its sum.
 *
 *
 * Example 1:
 *
 * Input: nums = [-2,1,-3,4,-1,2,1,-5,4] [-5, -3,-2,-1,1,1,2,4,4]
 * Output: 6
 * Explanation: [4,-1,2,1] has the largest sum = 6.
 */
public class MaxSubArray {

    public static void main(String[] args) {
        System.out.println(maxSubArray(new int[]{1, -10, 50}));
    }
    // TODO: There is an option to solve it with bruteForce by using a nested loop
    //        int maxSubarray = Integer.MIN_VALUE;
    //        for (int i = 0; i < nums.length; i++) {
    //            int currentSubarray = 0;
    //            for (int j = i; j < nums.length; j++) {
    //                currentSubarray += nums[j];
    //                maxSubarray = Math.max(maxSubarray, currentSubarray);
    //            }
    //        }
    //
    //        return maxSubarray;
    //    }
    //
    // YOU can start with sliding window, but there is no window, so it will be N2


    // BETTER SOLUTION
    //  // Initialize our variables using the first element.
    //        int currentSubarray = nums[0];
    //        int maxSubarray = nums[0];
    //
    //        // Start with the 2nd element since we already used the first one.
    //        for (int i = 1; i < nums.length; i++) {
    //            int num = nums[i];
    //            // If current_subarray is negative, throw it away. Otherwise, keep adding to it.
    //            currentSubarray = Math.max(num, currentSubarray + num);
    //            maxSubarray = Math.max(maxSubarray, currentSubarray);
    //        }
    //
    //        return maxSubarray;

    public static int maxSubArray(int[] nums) {
        // Initialize our variables using the first element.
        int current = 0; // TODO: you can declare to nums[0]
        int max = Integer.MIN_VALUE; // TODO: you can declare to nums[0]

        // Start with the 2nd element since we already used the first one.
        for (int i = 0; i < nums.length; i++) { //TODO you can start looping from 1
            int num = nums[i];
            // we need to determin current max value. To compare it with number and sum of number and current
            // what is bigger num or the current sum???
            // if current sum, then ignore negative number
            if (num > current+num) {
                current = num;
            } else {
                current = current + num;
            }
//            current = Math.max(num, current + num); // TODO you can simplify the above code to this
            // after that pick
            max = Math.max(max, current);
        }

        return max;
//        int leftPointer = 0, rightPointer = 1;
//        int max = Integer.MIN_VALUE;
//        int currentSum = nums[0];
//
//        while (leftPointer < nums.length-1) {
//            if (rightPointer >= nums.length ) {
//                leftPointer++;
//                currentSum = nums[leftPointer];
//                rightPointer = leftPointer;
//
//            }
//            System.out.println("right: " + rightPointer);
//            System.out.println("left: " + leftPointer);
//            if (rightPointer < nums.length && rightPointer != leftPointer) {
//                currentSum = currentSum + nums[rightPointer];
//            }
//
//            if (currentSum > max ) {
//                max = currentSum;
//            }
//            rightPointer++;
//            System.out.println(max);
//
//        }
//
//        return max;
    }

}
