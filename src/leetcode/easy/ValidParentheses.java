package leetcode.easy;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 *  Valid Parentheses
 *  https://leetcode.com/problems/valid-parentheses/
 * Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
 *
 * An input string is valid if:
 *
 * Open brackets must be closed by the same type of brackets.
 * Open brackets must be closed in the correct order.
 *
 *
 * Example 1:
 *
 * Input: s = "()"
 * Output: true
 */
public class ValidParentheses {
    // discuss like if there is only one type of parantheses. ()(()(
    // in this case we can create a pointer for the first Open parantheses and in case if there is another we can increment it
    // otherwise we can decrement the counter
    // but we have 3 different parantheses
    // so we need to search for valid constructions and remove them from the string
    // we can create a key value map to store all the symbols that we have
    // LET ME THINK dobavljaj skobki i rassuzhdaj, tipo this is like a QUEUE..but it will be FIFO,
    // but we need to check the last one, so it should be a Stack, so we can determine the last element
    // and we can have a map, where opening will be the key and closing the value and during the iteration
    // we will check if the paranthesis is closing
    // ((([[]

    public static void main(String[] args) {
        System.out.println(isValid("([)]"));
    }

    public static boolean isValid(String s) {
        Map<Character, Character> paranthesesMap = new HashMap<>();
        paranthesesMap.put('{', '}');
        paranthesesMap.put('(', ')');
        paranthesesMap.put('[', ']');
        Stack<Character> parStack = new Stack<>();
        for (Character c : s.toCharArray()) {
            if (parStack.isEmpty()) {
                parStack.push(c);
                continue;
            }
            Character currentTop = parStack.peek(); // peek allows to check the top element of the stack
            if (paranthesesMap.get(currentTop) == null) { // TODO if the top is closing bracket, then smth is wrong
                return false;
            }
            if (paranthesesMap.get(currentTop).equals(c)) { // check if opening bracket will be closed
                parStack.pop(); // pop removes object from top of the stack
            } else {
                parStack.push(c);
            }


        }
        // TODO possible to simplify:
        // If the stack still contains elements, then it is an invalid expression.
        //    return stack.isEmpty();
        if (parStack.isEmpty()) {
            return true;
        } else {
            return false;
        }


    }
 // TODO: some ideas for better solution:
    /**
     *   // Hash table that takes care of the mappings.
     *   private HashMap<Character, Character> mappings;
     *
     *   // Initialize hash map with mappings. This simply makes the code easier to read.
     *   public Solution() {
     *     this.mappings = new HashMap<Character, Character>();
     *     this.mappings.put(')', '(');
     *     this.mappings.put('}', '{');
     *     this.mappings.put(']', '[');
     *   }
     *
     *   public boolean isValid(String s) {
     *
     *     // Initialize a stack to be used in the algorithm.
     *     Stack<Character> stack = new Stack<Character>();
     *
     *     for (int i = 0; i < s.length(); i++) {
     *       char c = s.charAt(i);
     *
     *       // If the current character is a closing bracket.
     *       if (this.mappings.containsKey(c)) {
     *
     *         // Get the top element of the stack. If the stack is empty, set a dummy value of '#'
     *         char topElement = stack.empty() ? '#' : stack.pop();
     *
     *         // If the mapping for this bracket doesn't match the stack's top element, return false.
     *         if (topElement != this.mappings.get(c)) {
     *           return false;
     *         }
     *       } else {
     *         // If it was an opening bracket, push to the stack.
     *         stack.push(c);
     *       }
     *     }
     *
     *     // If the stack still contains elements, then it is an invalid expression.
     *     return stack.isEmpty();
     *   }
     */
}
