package leetcode.easy;


/**
 * Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.
 *
 * Assume the environment does not allow you to store 64-bit integers (signed or unsigned).
 *
 *
 *
 * Example 1:
 *
 * Input: x = 123
 * Output: 321
 */
public class ReverseInteger {

    public static void main(String[] args) {
        System.out.println(reverseInteger(1534236469));
    }
    // to convert to String, make a reverse, then back is back solution
    public static int reverseInteger(int x) {
        int reversed = 0;
        int pop;

        while (x != 0) {
            pop = x % 10; // remainder will always be the last number
            x /= 10; // devide X by 10, to get the next number in the next iteration

            reversed = (reversed * 10) + pop;
            if (reversed > Integer.MAX_VALUE || reversed<Integer.MIN_VALUE) {
                return 0;
            }
        }


        return reversed;
    }
}
