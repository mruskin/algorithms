package leetcode.easy;

import java.util.Arrays;
import java.util.List;

/**
 *https://www.algoexpert.io/questions/Validate%20Subsequence
 */
public class ValidateSequence {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1,2,3,4, 5);
        List<Integer> list2 = Arrays.asList(2,3,5);
        System.out.println(isValidSubsequence(list,list2));

    }

    public static boolean isValidSubsequence(List<Integer> array, List<Integer> sequence) {
        // Write your code here.
        // 123 23
        //
        boolean result;
        int y = 0;
        int l = sequence.size();
        for (Integer i : array) {
            if (sequence.get(y).equals(i)) {
                y++;
            }
        }
        return y == l;
    }


}
