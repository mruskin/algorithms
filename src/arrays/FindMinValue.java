package arrays;

/** EDUCATIVE
 * Problem Statement #
 * In this problem, you have to implement the int findMinimum(int[] arr) method, which will traverse the whole array and find the smallest number in the array.
 *
 * Method Prototype #
 * int findMinimum(int[] arr)
 * Output #
 * The smallest number in the array.
 *
 * Sample Input #
 * arr = {9, 2, 3, 6}
 * Sample Output #
 * 2
 */
public class FindMinValue {

    public static void main(String[] args) {
        System.out.println(findMinimum(new int[]{100, 200, 300, 100, 12, 13, 14, 1}));
    }

    public static int findMinimum(int[] arr) {
        int temp = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < temp) {
                temp = arr[i];
            }
        }
        return temp;
    }
}
