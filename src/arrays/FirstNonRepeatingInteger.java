package arrays;

/** EDUCATIVE
 * Problem Statement #
 * In this problem, you have to implement the int findFirstUnique(int[] arr) method that will look for a first unique integer, which appears only once in the whole array. The function returns -1 if no unique number is found.
 *
 * Method Prototype #
 * int findFirstUnique(int[] arr)
 * Output #
 * The first unique element in the array.
 *
 * Sample Input #
 * arr = {9, 2, 3, 2, 6, 6}
 * Sample Output #
 * 9
 */
public class FirstNonRepeatingInteger {

    public static void main(String[] args) {
        System.out.println(findFirstUnique(new int[]{5, 4, 5, 4, 1, 2, 2, 3, 3}));

    }

    public static int findFirstUnique(int[] arr) {
//        int counter = 0;
//        int val = arr[0];
//        int i = 1;
//        while (counter!=1 && i!=arr.length) {
//            if (arr[i]==val) {
//                counter++;
//            }
//            i++;
//        }
        int counter = 0;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (i!=j && arr[i] == arr[j]) {
                    counter++;
                }
            }
            if (counter == 0) {
                return arr[i];
            }
            counter=0;
        }
        return 0;
    }
}
