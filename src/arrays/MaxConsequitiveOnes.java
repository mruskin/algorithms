package arrays;

/**
 * LEETCODE
 * Input: nums = [1,1,0,1,1,1]
 * Output: 3
 * Explanation: The first two digits or the last three digits are consecutive 1s.
 * The maximum number of consecutive 1s is 3.
 *
 */
public class MaxConsequitiveOnes {

    public static void main(String[] args) {
        System.out.println(findCons(new int[]{1,1,0,1,1,1, 0, 0, 1, 1, 2, 2,2 ,1, 1, 1, 1}));
    }

    public static int findCons(int[] arr) {
        int currentMaxConsec = 0;
        int lastMaxConsec = 0;
        int one = 1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == one) {
                currentMaxConsec++;
                if (currentMaxConsec > lastMaxConsec) {
                    lastMaxConsec = currentMaxConsec;
                }
            } else {
                currentMaxConsec = 0;
            }
        }
        return lastMaxConsec;
    }
}
