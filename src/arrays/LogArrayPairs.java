package arrays;

public class LogArrayPairs {

    public static void main(String[] args) {
        int[] pairs = new int[]{ 1, 2, 3, 4, 5};
        logPairs(pairs);
    }

    public static void logPairs(int[] pairs) {
        int length = pairs.length;
        for (int i = 0; i < length; i++) {
            int tempI = pairs[i];
            for (int j = 0; j < length; j++) {
                System.out.println("pair: [".concat(String.valueOf(tempI)).concat(", ").concat(String.valueOf(pairs[j]).concat(" ]")));
            }
        }
    }
}
