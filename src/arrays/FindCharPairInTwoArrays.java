package arrays;

import java.util.HashSet;
import java.util.Set;

//O(a + b)
public class FindCharPairInTwoArrays {

    public static void main(String[] args) {
        char [] first = new char[]{'a', 'a', 'c'};
        char [] second = new char[]{'g', 'f', 'a'};
        System.out.println(hasPairOfChar(first, second));
    }

    public static boolean hasPairOfChar(char[] first, char[] second) {
        Set<Character> firstMap = new HashSet<>();

        for (int i = 0; i < first.length; i++) {
            if (!firstMap.contains(first[i])) {
                firstMap.add(first[i]);
            }
        }
        System.out.println(firstMap);
        for(int j = 0; j < second.length; j++) {
            if (firstMap.contains(second[j])) {
                return true;
            }
        }
        return false;
    }
}
