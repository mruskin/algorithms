package arrays;

import java.util.Arrays;

/**EDUCATIVE
 * In this problem, given two sorted arrays, you have to implement the int[] mergeArrays(int[] arr1, int[] arr2) method,
 * which returns an array consisting of all elements of both arrays in a sorted way.
 * Merged array consisting of all elements of both arrays in a sorted way.
 *
 * Sample Input #
 * arr1 = {1, 3, 4, 5}
 * arr2 = {2, 6, 7, 8}
 * Sample Output #
 * arr = {1, 2, 3, 4, 5, 6, 7, 8}
 */
public class MergeTwoSortedArrays {

    public static void main(String[] args) {
        int[] arr1 = new int[]{0, 2, 3, 4, 7, 9, 11};
        int[] arr2 = new int[]{1, 5, 6, 8, 10};
        System.out.println(Arrays.toString(mergeArrays(arr1, arr2)));
    }
//    0       2
//    1       5
//    i i+1   i   i+1
//    0 1     2   5

    public static int[] mergeArrays(int[] arr1, int[] arr2) {
        int result[] = new int[arr1.length + arr2.length];
        int j =0;
        int k =0;
        for (int i = 0; i < result.length; i++) {
            if (j < arr1.length && (k == arr2.length || arr1[j] < arr2[k])) {
                result[i] = arr1[j];
                j++;
            } else {
                result[i] = arr2[k];
                k++;
            }

        }
        return result;
    }
}
