package arrays;

import java.lang.reflect.Array;
import java.util.Arrays;

/** EDUCATIVE
 * Input #
 * An array with integers.
 *
 * Output #
 * An array with only odd integers.
 *
 * Sample Input #
 * arr = {1, 2, 4, 5, 10, 6, 3}
 * Sample Output #
 * arr = {1, 5, 3}
 */
public class RemoveEvenIntegers {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(removeEven(new int[]{2, 3, 4, 5, 6, 8, 5, 7})));
    }

    public static int[] removeEven(int[] arr) {
        int evenNums = 0;
        for (int i = 0; i < arr.length; i ++) {
            if (arr[i] % 2 != 0) {
                evenNums++;
            }
        }
        int[] evenArr = new int[evenNums];
        int j = 0;
        for (int i = 0; i < arr.length; i ++) {
            if (arr[i] % 2 != 0) {
                evenArr[j]=arr[i];
                j++;
            }
        }
        return evenArr;
    }
}
