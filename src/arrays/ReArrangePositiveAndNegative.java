package arrays;

import java.util.Arrays;

/** EDUCATIVE
 * In this problem, you have to implement the void reArrange(int[] arr) method, which will sort the elements, such that all the negative elements appear at the left and positive elements appear at the right.
 *
 * Note: Consider 0 as a positive number.
 *
 * Method Prototype #
 * void reArrange(int[] arr)
 * Output #
 * A sorted array with negative elements at the left and positive elements at the right.
 *
 * Sample Input #
 * arr = {10, -1, 20, 4, 5, -9, -6}
 * Sample Output #
 * arr = {-1, -9, -6, 10, 20, 4, 5}
 * Note: Order of the numbers doesn’t matter.
 *
 * {-1, -9, -6, 10, 20, 4, 5} = {-9, -1, -6, 10, 4, 20, 5}
 */
public class ReArrangePositiveAndNegative {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(reArrange(new int[]{10, -1, 20, 4, 5, -9, -6})));
    }

    public static int[] reArrange(int[] arr) {
        int negArr[] = new int[arr.length];
        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0) {
                negArr[counter] = arr[i];
                counter++;
            }
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= 0) {
                negArr[counter] = arr[i];
                counter++;
            }
        }
        for (int i = 0; i < arr.length; i++) {
            arr[i] = negArr[i];
        }
        return arr;
    }

}
