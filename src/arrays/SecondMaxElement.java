package arrays;

/** EDUCATIVE
 * In this problem, you have to implement the int findSecondMaximum(int[] arr) method, which will traverse the whole array and return the second largest element present in the array.
 *
 * Assumption: Array should contain at least two unique elements.
 *
 * Method Prototype #
 * int findSecondMaximum(int[] arr)
 * Output #
 * Second largest element present in the array.
 *
 * Sample Input #
 * arr = {9,2,3,6}
 * Sample Output #
 * 6
 */
public class SecondMaxElement {

    public static void main(String[] args) {
        System.out.println(findSecondMaximum(new int[]{1, 3, 50, 4, 14, 13, 25}));
    }

    public static int findSecondMaximum(int[] arr)
    {
        int max = Integer.MIN_VALUE;;
        int secondmax = Integer.MIN_VALUE;

        // Find the maximum value in the array by comparing each index with max
        // If an element is greater than max then update max to be equal to it
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max)
                max = arr[i];

        }//end of for-loop

        // Find the second maximum value by comparing each index with secondmax
        // If an element is greater than secondmax and not equal to previously
        // calculated max, then update secondmax to be equal to that element.
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > secondmax && arr[i] < max)
                secondmax = arr[i];

        }//end of for-loop

        return secondmax;
    }

//ANOTHER SOLUTION WITH ONE LOOP
//    int max = Integer.MIN_VALUE;;
//    int secondmax = Integer.MIN_VALUE;
//
//    // Keep track of Maximum value, whenever the value at an array index is greater
//    // than current Maximum value then make that max value 2nd max value and
//    // make that index value maximum value
//    for (int i = 0; i < arr.length; i++) {
//        if (arr[i] > max) {
//            secondmax = max;
//            max = arr[i];
//        }
//        else if (arr[i] > secondmax && arr[i] != max) {
//            secondmax = arr[i];
//        }
//    }//end of for-loop
//
//    return secondmax;
}
