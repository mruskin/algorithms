package arrays;

/**
 * LEETCODE
 * Find Numbers with Even Number of Digits
 * Given an array nums of integers, return how many of them contain an even number of digits.
 *
 *
 * Example 1:
 *
 * Input: nums = [12,345,2,6,7896]
 * Output: 2
 * Explanation:
 * 12 contains 2 digits (even number of digits).
 * 345 contains 3 digits (odd number of digits).
 * 2 contains 1 digit (odd number of digits).
 * 6 contains 1 digit (odd number of digits).
 * 7896 contains 4 digits (even number of digits).
 * Therefore only 12 and 7896 contain an even number of digits.
 */
public class FindNumbersWithEvenNumbersOfDigits {
    public static void main(String[] args) {
        System.out.println(evenNumber(new int[] {12,345,2,6,7896, 2, 2, 22}));
    }

    public static int evenNumber(int [] arr) {
        int evenCounter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (String.valueOf(arr[i]).length() % 2 == 0) {
                evenCounter++;
            }
        }
        return evenCounter;
    }
}
