package arrays;

import java.util.HashSet;
import java.util.Set;
// O(n)
public class FindSumInArray {
    public static void main(String[] args) {
        int[] arr = new int[] {6, 3, 2, 2};
        System.out.println(findSumInArray(arr, 8));
    }

    public static boolean findSumInArray(int[] arr, int sum) {
        Set<Integer> elAndSumDiff = new HashSet<>();
        for (int i = 0; i < arr.length; i++) {
            if (elAndSumDiff.contains(arr[i])) {
                return true;
            }
            elAndSumDiff.add(Integer.valueOf(sum - arr[i]));
        }
        return false;
    }
}
