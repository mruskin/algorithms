package arrays;

import java.util.Arrays;

/**
 * In this problem, you have to implement the void rotateArray(int[] arr) method, which takes an arr and rotate it right by 1. This means that the right-most elements will appear at the left-most position in the array.
 *
 * Method Prototype #
 * void rotateArray(int[] arr)
 * Output #
 * Array rotated by one element from right to left
 *
 * Sample Input #
 * arr = {1, 2, 3, 4, 5}
 * Sample Output #
 * arr = {5, 1, 2, 3, 4}
 */
public class RightRotateByOneIndex {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(rotateArray(new int[]{1, 2, 3, 4, 5})));
    }

    public static int[] rotateArray(int[] arr){
        //Store last element of Array.
        //Start from the Second last and Right Shift the Array by one.
        //Store the last element saved on the first index of the Array.
        int lastElement = arr[arr.length - 1];

        for (int i = arr.length - 1; i > 0; i--) {

            arr[i] = arr[i - 1];
        }

        arr[0] = lastElement;
        return arr;


        //NOT SO GOOD SOLUTION
//        int firstElement = arr[0];
//        int lastElement = arr[arr.length-1];
//        int previous = firstElement;
//
//        for (int i = 1; i < arr.length; i ++) {
//            int temp = arr[i];
//
//                arr[i] = previous;
//
//            previous = temp;
//
//        }
//        arr[0] = lastElement;
//        return arr;


    }
}
