package arrays;

import java.util.Arrays;

/**
 * int[] findSum(int[] arr, int n)
 * Output #
 * An array with two integers a and b that add up to a given number.
 *
 * Sample Input #
 * arr = {1, 21, 3, 14, 5, 60, 7, 6}
 * value = 27
 * Sample Output #
 * arr = {21, 6} or {6, 21}
 */
public class TwoNumSum {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(twoSum(new int[]{1, 2, 100, 200, 5, 2}, 4)));

    }

    public static int[] twoSum(int[] arr, int sum) {
        int[] result = new int[2];
        for (int i = 0; i<arr.length; i++) {

            for (int j = i+1; j<arr.length;j++) {
                if (arr[i] + arr[j] == sum) {
                    result[0] = arr[i];
                    result[1] = arr[j];
                    return result;
                }
            }
        }
        return result;
    }
}
