package arrays;

public class SortedArrayBinarySearch {

    static int binSearch(int[] a, int key) {
        int low = 0;
        int high = a.length-1;
        while (low <= high) {
            int mid = low + ((high - low) / 2);
            if (a[mid] == key) {
                return mid;
            }
            if (a[mid] > key) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }

        return -1;
    }
}
