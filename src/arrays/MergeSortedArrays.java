package arrays;

import java.util.Arrays;

public class MergeSortedArrays {

    public static void main(String[] args) {
        int[] arr1 = new int[]{0, 2, 3, 4, 7, 9, 11};
        int[] arr2 = new int[]{1, 5, 6, 8, 10};
        System.out.println(Arrays.toString(mergeSortedArray(arr1, arr2)));
    }

    public static int[] mergeSortedArray(int[] array1, int[] array2) {
        final int[] mergedArray = new int[array1.length + array2.length];
        int j = 0, k = 0;
        for (int i = 0; i < mergedArray.length; i++) {
            if (j < array1.length && (k == array2.length || array1[j] < array2[k])) { // in case if K is on max, so it will not go to other condition
                mergedArray[i] = array1[j];
                j++;
            } else {
                mergedArray[i] = array2[k];
                k++;
            }
        }
        return mergedArray;
    }
 }
