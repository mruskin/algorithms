package tests;

import java.util.function.Consumer;

public class Test {

    public static void main(String[] args) {
        Consumer<String> s1 = arg -> System.out.println(arg);
        s1.accept("lol");
    }

}
