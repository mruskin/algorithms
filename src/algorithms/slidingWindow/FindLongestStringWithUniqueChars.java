package algorithms.slidingWindow;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Given a sample input of “abcabcbb”, the expected output was “3” and the determining string was “abc.”
 * Based on this information, I know that I was going to be given a string and that my code should return a number.
 * Are there other sample inputs and expected outputs that were given? According to the challenge,
 * two other tests were given: “bbbbb” with expected return of 1 from “b” and “pwwkew” with expected
 * return of 3 from “wke.”
 * My list to translate the code
 * Address possible edge cases. The problem didn’t tell me if the string given would be less than a certain length.
 * Knowing that an empty string will return 0 (zero), I returned a 0. If the length of the string was 1 (one),
 * I returned 1.
 * Create a variable for “leftPointer”, “rightPointer”, and “max” to hold value for the longest string with
 * unique characters. I also created a Set<Character> data structure to hold the unique characters as I
 * traversed the string.
 * Iterate through the string with the “rightPointer” and use the input string’s length as the conditional value.
 * For each character that the rightPointer increments through, check to see if that character is contained in
 * the Set data structure. If it is not, add it to the Set. Calculate the max value by getting the size of the Set.
 * If the size of the set is larger than the value in max, set max equal to the size of the set.
 * If the “rightPointer” encounters a character that is already contained in the Set, remove that character and
 * increment the “leftPointer.”
 * Continue the process until the “rightPointer” reaches the end of the string. By that point,
 * the max should be calculated and the Set should include the unique characters that the problem was looking to solve.
 */
public class FindLongestStringWithUniqueChars {

    public static void main(String[] args) {
        System.out.println(longestUniqueString("pwwkew"));
    }

    public static int longestUniqueString(String s) {
        if (s.length() < 1) {
            return 0;
        }
        if (s.length() == 1) {
            return 1;
        }
        int leftPointer = 0, rightPointer = 0, max = 0;
        Set<Character> set = new HashSet<>();
        while (rightPointer < s.length()) {
            if (!set.contains(s.charAt(rightPointer))) {
                set.add(s.charAt(rightPointer++));
                max = Math.max(max, set.size());
            } else {
                set.remove(s.charAt(leftPointer++));
            }
        }
        return max;
        //NOT SO GOOD SOLIUTION
//        Set<Character> set = new HashSet<>();
//        int leftPointer = 0, rightPointer = 0, longest = 0;
//
//        while (leftPointer < input.length() && rightPointer < input.length()) {
//            if (!set.contains(input.charAt(rightPointer))) {
//                set.add(input.charAt(rightPointer));
//                rightPointer++;
//                if (set.size() > longest) {
//                    longest = set.size();
//                }
//            } else {
//                set.clear();
//                leftPointer++;
//                rightPointer=leftPointer;
//            }
//
//        }
//
//        return longest;
    }

}
