package math;

import java.util.ArrayList;
import java.util.List;

/**
 * Given an integer n, return a string array answer (1-indexed) where:
 *
 * answer[i] == "FizzBuzz" if i is divisible by 3 and 5.
 * answer[i] == "Fizz" if i is divisible by 3.
 * answer[i] == "Buzz" if i is divisible by 5.
 * answer[i] == i if non of the above conditions are true.
 */
public class FizzBuzz {

    public static void main(String[] args) {
        System.out.println(fizzBuzz(15));
    }

    public static List<String> fizzBuzz(int n) {
        List<String> answer = new ArrayList<>();
        if (n % 5 == 0 && n % 3 == 0) {
            answer.add("FizzBuzz");
            return answer;
        } else if (n % 5 == 0 ) {
            answer.add("Bizz");
            return answer;
        } else if (n % 3 == 0 ) {
            answer.add("Fizz");
            return answer;
        }

        answer.add(String.valueOf(n));
        return answer;
    }
}
