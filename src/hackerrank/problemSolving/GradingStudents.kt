package hackerrank.problemSolving

import java.util.*

/*
 * Complete the gradingStudents function below.
 * https://www.hackerrank.com/challenges/grading/problem
 */

fun gradingStudents(grades: List<Int>): List<Int> {
    /*
     * Write your code here.
     */
    val result: MutableList<Int> = mutableListOf<Int>()
    for (grade in grades.toList()) {
        if (grade < 38) {
            result.add(grade)
            continue
        }
        if (grade%5 >= 3) {
            result.add(grade + (5 - grade%5))
        } else {
            result.add(grade)
        }
    }
    return result
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val n = scan.nextLine().trim().toInt()

    val grades: MutableList<Int> = mutableListOf()
    for (gradesItr in 0 until n) {
        val gradesItem = scan.nextLine().trim().toInt()
        grades.add(gradesItem)
    }

    val result = gradingStudents(grades)

    println(result.joinToString("\n"))
}
