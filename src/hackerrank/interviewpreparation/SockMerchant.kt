package hackerrank.interviewpreparation

import java.util.*

// Complete the sockMerchant function below.
// https://www.hackerrank.com/challenges/sock-merchant/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup
fun sockMerchant(socks: List<Int>): Int {
    val map: HashMap<Int, Int> = hashMapOf()
    var result = 0
    for (sock in socks) {
        if (!map.containsKey(sock)) {
            map.put(sock, 1)
        } else {
            result++;
            map.remove(sock)
        }
    }
    return result
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val n = scan.nextLine().trim().toInt() // leave it for tests
    val ar = scan.nextLine().trim().split(" ").map { it.trim().toInt() }

    val result = sockMerchant(ar)

    println(result)
}
