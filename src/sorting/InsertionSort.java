package sorting;

import java.util.Arrays;

public class InsertionSort {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(insertionSort(new int[]{5, 2, 22, 10, 1, 3, 7, 13, 8, 4})));
    }

    public static int[] insertionSort(int[] nums) {
        for (int i = 1; i < nums.length; i++) {
            int temp = nums[i];
            int j = i - 1;
            System.out.println(j);
            while (j >= 0 && nums[j] > temp) {
                nums[j + 1] = nums[j];
                j = j - 1;
            }
            nums[j + 1] = temp;

        }

        return nums;
    }
}
