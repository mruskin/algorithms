package sorting;

import java.util.Arrays;

public class SelectionSort {

    public static void main(String[] args) {
        int[] arr = new int[]{5, 2, 22, 10, 1, 3, 7, 13, 8, 4};
        System.out.println(Arrays.toString(selectionSort(arr)));
    }

    public static int[] selectionSort(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            int min = i; //assume, that i is min element
            int temp = arr[i];
            for (int j = i +1; j < arr.length; j++) {
               if (arr[j] < arr[min]) {
                   min = j;
               }
            }
            arr[i] = arr[min];
            arr[min] = temp;
        }
        return arr;
    }
}
