package uber.arraysAndStrings;

import java.util.*;

/**
 * https://leetcode.com/explore/interview/card/uber/289/array-and-string/1686/
 * Minimum Window Substring
 * Given two strings s and t of lengths m and n respectively, return the minimum window in s which will contain all the characters in t. If there is no such window in s that covers all characters in t, return the empty string "".
 *
 * Note that If there is such a window, it is guaranteed that there will always be only one unique minimum window in s.
 *
 *
 *
 * Example 1:
 *
 * Input: s = "ADOBECODEBANC", t = "ABC"
 * Output: "BANC"
 */


/**
 * 1. put all T elements to a MAP
 * 2.
 * 3. Search for the desired window
 *  3.1 iterate over all chars array
 *
 *
 */
public class MinimumWindowSubstring {

    public static void main(String[] args) {
        System.out.println(minWindow("ADOBECODEBANC", "ABC"));
    }

    public static String minWindow(String s, String t) {
        // TODO: don't add this check
        if (s.length() == 0 || t.length() == 0) {
            return "";
        }

        // Dictionary which keeps a count of all the unique characters in t.
        Map<Character, Integer> windowMap = new HashMap<Character, Integer>();
        for (int i = 0; i < t.length(); i++) {
            int val = windowMap.getOrDefault(t.charAt(i), 0);
            windowMap.put(t.charAt(i), val + 1);
        }

        // currentMax is used to keep track of how many unique characters in t
        // are present in the current window in its desired frequency.
        // e.g. if t is "AABC" then the window must have two A's, one B and one C.
        // Thus currentMax would be = 3 when all these conditions are met.
        int currentMax = 0;
        // Number of unique characters in t, which need to be present in the desired window.
        int required = windowMap.size();
        // Left and Right pointer
        int leftPointer = 0, rightPointer = 0;

        // Dictionary which keeps a count of all the unique characters in the current window.
        Map<Character, Integer> windowCounts = new HashMap<Character, Integer>();

        // ans list of the form (window length, left, right)
        int[] ans = {-1, 0, 0};

        while (rightPointer < s.length()) {
            // Add one character from the right to the window
            char c = s.charAt(rightPointer);
            int count = windowCounts.getOrDefault(c, 0);
            windowCounts.put(c, count + 1);

            // If the frequency of the current character added equals to the
            // desired count in t then increment the currentMax count by 1.
            if (windowMap.containsKey(c) && windowCounts.get(c).intValue() == windowMap.get(c).intValue()) {
                currentMax++;
            }

            // Try and contract the window till the point where it ceases to be 'desirable'.
            while (leftPointer <= rightPointer && currentMax == required) {
                c = s.charAt(leftPointer);
                // Save the smallest window until now.
                if (ans[0] == -1 || rightPointer - leftPointer + 1 < ans[0]) {
                    ans[0] = rightPointer - leftPointer + 1;
                    ans[1] = leftPointer;
                    ans[2] = rightPointer;
                }

                // The character at the position pointed by the
                // `Left` pointer is no longer a part of the window.
                windowCounts.put(c, windowCounts.get(c) - 1);
                if (windowMap.containsKey(c) && windowCounts.get(c).intValue() < windowMap.get(c).intValue()) {
                    currentMax--;
                }

                // Move the left pointer ahead, this would help to look for a new window.
                leftPointer++;
            }

            // Keep expanding the window once we are done contracting.
            rightPointer++;
        }

        return ans[0] == -1 ? "" : s.substring(ans[1], ans[2] + 1);


//        String res = "";
//        int leftPointer = 0;
//        int rightPointer = 0;
//        char[] c = s.toCharArray();
//        List<Character> chars = new ArrayList<>();
//        for (char ch : t.toCharArray()) {
//            chars.add(ch);
//        }
//        int uniqueCounter = 0;
//        Set<Character> set = new HashSet<>();
//        set.addAll(chars);
//        while (rightPointer < c.length) {
//            if (!set.contains(c[rightPointer])) {
//
//
//            } else {
//                res = res.concat("c[leftPointer]");
//            }
//        }
//
//        return res;
    }

}
