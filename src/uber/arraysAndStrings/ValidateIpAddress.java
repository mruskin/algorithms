package uber.arraysAndStrings;

import java.util.Arrays;

/**
 * Validate IP Address
 * https://leetcode.com/explore/interview/card/uber/289/array-and-string/1687/
 *
 * Given a string IP, return "IPv4" if IP is a valid IPv4 address, "IPv6" if IP is a valid IPv6 address or "Neither" if IP is not a correct IP of any type.
 *
 * A valid IPv4 address is an IP in the form "x1.x2.x3.x4" where 0 <= xi <= 255 and xi cannot contain leading zeros. For example, "192.168.1.1" and "192.168.1.0" are valid IPv4 addresses but "192.168.01.1", while "192.168.1.00" and "192.168@1.1" are invalid IPv4 addresses.
 *
 * A valid IPv6 address is an IP in the form "x1:x2:x3:x4:x5:x6:x7:x8" where:
 *
 * 1 <= xi.length <= 4
 * xi is a hexadecimal string which may contain digits, lower-case English letter ('a' to 'f') and upper-case English letters ('A' to 'F').
 * Leading zeros are allowed in xi.
 * For example, "2001:0db8:85a3:0000:0000:8a2e:0370:7334" and "2001:db8:85a3:0:0:8A2E:0370:7334" are valid IPv6 addresses, while "2001:0db8:85a3::8A2E:037j:7334" and "02001:0db8:85a3:0000:0000:8a2e:0370:7334" are invalid IPv6 addresses.
 *
 *
 *
 * Example 1:
 *
 * Input: IP = "172.16.254.1"
 * Output: "IPv4"
 * Explanation: This is a valid IPv4 address, return "IPv4".
 */
public class ValidateIpAddress {

    public static void main(String[] args) {
        System.out.println(validIPAddress("12..33.4"));
    }
    // TODO: you can mention, that there is InetAddress library in java to verify IP address
//    public String validIPAddress(String IP) {
//        try {
//            return (InetAddress.getByName(IP) instanceof Inet6Address) ? "IPv6": "IPv4";
//        } catch(Exception e) {}
//        return "Neither";
//    }

    // TODO: mention, that it is possible to solve it only with regexp and it will be the fastest solution
//    String chunkIPv4 = "([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])";
//    Pattern pattenIPv4 =
//            Pattern.compile("^(" + chunkIPv4 + "\\.){3}" + chunkIPv4 + "$");
//
//    String chunkIPv6 = "([0-9a-fA-F]{1,4})";
//    Pattern pattenIPv6 =
//            Pattern.compile("^(" + chunkIPv6 + "\\:){7}" + chunkIPv6 + "$");
//
//    public String validIPAddress(String IP) {
//        if (pattenIPv4.matcher(IP).matches()) return "IPv4";
//        return (pattenIPv6.matcher(IP).matches()) ? "IPv6" : "Neither";
//    }
    // TODO: possible to brutforce with if/else and also with regexp
//    Now the problem is reduced to the construction of pattern to match each chunk. It's an integer in range (0, 255), and the leading zeros are not allowed. That results in five possible situations:
//
//    Chunk contains only one digit, from 0 to 9.
//
//    Chunk contains two digits. The first one could be from 1 to 9, and the second one from 0 to 9.
//
//    Chunk contains three digits, and the first one is 1. The second and the third ones could be from 0 to 9.
//
//    Chunk contains three digits, the first one is 2 and the second one is from 0 to 4. Then the third one could be from 0 to 9.
//
//    Chunk contains three digits, the first one is 2, and the second one is 5. Then the third one could be from 0 to 5.

    public static String validIPAddress(String IP) {
        String res = "Neither";
        if (IP.chars().filter(ch -> ch == '.').count() == 3) {
            String[] ipV4 = IP.split("\\.", -1); // TODO: in case of . use double backslash
            // ALSO don't put -1 it is added for trailing cases 12.33.4. like this
            for (String num : ipV4) {
                // Validate integer in range (0, 255):
                // 1. length of chunk is between 1 and 3
                if (num.length() == 0 || num.length() > 3) return res;
                // 2. no extra leading zeros
                if (num.charAt(0) == '0' && num.length() > 1) return res;

                // 3. only digits are allowed
                int val;
                try {
                    val = Integer.parseInt(num);
                } catch (Exception e) {
                    return res;
                }
                //// 3. only digits are allowed TODO: another option to check if there are only didgets
                //                for (char ch : num.toCharArray()) {
                //                    if (! Character.isDigit(ch)) return res;
                //                }
                if (val < 0 || val > 255) {
                    return res;
                }
            }
            return "IPv4";
        } else if (IP.chars().filter(ch -> ch == ':').count() == 7) {
            String[] ipV6 = IP.split(":", -1);
            String hexdigits = "0123456789abcdefABCDEF"; // TODO: do not write it, maybe try regex at first
            for (String num : ipV6) {
                if (num.length() > 4 || num.length() < 1 || !num.matches("-?[0-9a-fA-F]+")) {
                    return res;
                }
                    // TODO: another option to calculate hexdigits
                // 2. only hexdigits are allowed: 0-9, a-f, A-F
//                for (Character ch : num.toCharArray()) {
//                    if (hexdigits.indexOf(ch) == -1) return "Neither";
//                }
            }
            return  "IPv6";
        }


        return res;
    }

}
