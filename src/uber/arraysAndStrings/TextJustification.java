package uber.arraysAndStrings;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * https://leetcode.com/explore/interview/card/uber/289/array-and-string/1685/
 *
 * Given an array of words and a width maxWidth, format the text such that each line has exactly maxWidth characters and is fully (left and right) justified.
 *
 * You should pack your words in a greedy approach; that is, pack as many words as you can in each line. Pad extra spaces ' ' when necessary so that each line has exactly maxWidth characters.
 *
 * Extra spaces between words should be distributed as evenly as possible. If the number of spaces on a line do not divide evenly between words, the empty slots on the left will be assigned more spaces than the slots on the right.
 *
 * For the last line of text, it should be left justified and no extra space is inserted between words.
 *
 * Note:
 *
 * A word is defined as a character sequence consisting of non-space characters only.
 * Each word's length is guaranteed to be greater than 0 and not exceed maxWidth.
 * The input array words contains at least one word.
 *
 * Input: words = ["This", "is", "an", "example", "of", "text", "justification."], maxWidth = 16
 * Output:
 * [
 *    "This    is    an",
 *    "example  of text",
 *    "justification.  "
 * ]
 */
public class TextJustification {

    public static void main(String[] args) {
        System.out.print((fullJustify(new String[]{"This", "is", "an", "example", "of", "text", "justification."}, 16)));
    }

    /**
     * We start with left being the first word.
     *
     * findRight: Then we greedily try to go as far right as possible until we fill our current line.
     *
     * Then we justify one line at a time.
     *
     * justify: In all cases we pad the right side with spaces until we reach max width for the line;
     *
     * If it's one word then it is easy, the result is just that word.
     * If it's the last line then the result is all words separated by a single space.
     * Otherwise we calculate the size of each space evenly and if there is a remainder we distribute an extra space until it is gone.
     * @param words
     * @param maxWidth
     * @return
     */




    // 1. iterate over the array of words
    // 2. find max available Right Element (maxWidth)
        // 2.1 iterate over the words array
        // 2.2 Until the length of the line
    // 3. add found line to the result List
    // 3.1 Justify the line that was added
    // 3.2 in all cases add spaces to right until we reach maxWidth
    // 3.3 if one word, when result is just this word
    // 3.4 if it is last line then the result is all words separated by a single space.
    // 3.5 Otherwise we calculate the size of each space evenly and if there is a remainder we distribute an extra space until it is gone.
    // 4. got to the max right element
    public static List<String> fullJustify(String[] words, int maxWidth) {
        List<String> result = new ArrayList<>();
        int left = 0;

        while (left < words.length) { // 1. iterate over the array of words to justify one line at a time
            int right = findRight(left, words, maxWidth); // 2. find max available Right Element (maxWidth)
            result.add(justify(left, right, words, maxWidth));// 3. add found line to the result List
            left = right + 1; // 4. got to the max right element
        }

        return result;
    }

    private static int findRight(int left, String[] words, int maxWidth) {
        int right = left; // right should be equal to left to track the position
        int currentLengthOfLine = words[right++].length(); // counter for sum of elements

        while (right < words.length /*&& (sum + 1 + words[right].length()) <= maxWidth*/) {// check the end of loop
            // TODO:  put <= instead of >
            if (words[right].length() + currentLengthOfLine + 1 > maxWidth) break; // TODO: REMOVE 1 and add it later like a mistake
            currentLengthOfLine += words[right].length() + 1;
            right++;
        }

        return right - 1;
    }

    private static String justify(int left, int right, String[] words, int maxWidth) {
        if (right - left == 0) return padResult(words[left], maxWidth);

        boolean isLastLine = right == words.length - 1;
        int numSpaces = right - left;
        int totalSpace = maxWidth - wordsLength(left, right, words);

        String space = isLastLine ? " " : blank(totalSpace / numSpaces);
        int remainder = isLastLine ? 0 : totalSpace % numSpaces;

        StringBuilder result = new StringBuilder();
        for (int i = left; i <= right; i++)
            result.append(words[i])
                    .append(space)
                    .append(remainder-- > 0 ? " " : "");

        return padResult(result.toString().trim(), maxWidth);
    }

    private static int wordsLength(int left, int right, String[] words) {
        int wordsLength = 0;
        for (int i = left; i <= right; i++) wordsLength += words[i].length();
        return wordsLength;
    }

    private static String padResult(String result, int maxWidth) {
        return result + blank(maxWidth - result.length());
    }

    private static String blank(int length) {
        return new String(new char[length]).replace('\0', ' ');
    }

}
