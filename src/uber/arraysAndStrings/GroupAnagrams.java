package uber.arraysAndStrings;

import java.util.*;

/**
 * https://leetcode.com/explore/interview/card/uber/289/array-and-string/1684/
 * Given an array of strings strs, group the anagrams together. You can return the answer in any order.
 *
 * An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.
 *
 *
 *
 * Example 1:
 *
 * Input: strs = ["eat","tea","tan","ate","nat","bat"]
 * Output: [["bat"],["nat","tan"],["ate","eat","tea"]]
 */
public class GroupAnagrams {

    public static void main(String[] args) {
        System.out.println(groupAnagrams(new String[] {"eat","tea","tan","ate","nat","bat"}));
    }

   // solution is to sort a string and will give us the same string
    // create a map with a string as a key and List as value
    // iterate over array of strings
    // create and sort array of chars
    // use sorted String as a Key
    //  add string to a map
    public static List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List> ans = new HashMap<>();
        for (String s : strs) {
            char[] stringOfChars = s.toCharArray();
            Arrays.sort(stringOfChars);
            String key = String.valueOf(stringOfChars);
            if (!ans.containsKey(key)) ans.put(key, new ArrayList());
            ans.get(key).add(s);
        }
        return new ArrayList(ans.values());




//        if (strs.length == 0) return new ArrayList<>();
//        Map<String, List> ans = new HashMap<>();
//        for (String s : strs) {
//            char[] ca = s.toCharArray();
//            Arrays.sort(ca);
//            String key = String.valueOf(ca);
//            if (!ans.containsKey(key)) ans.put(key, new ArrayList());
//            ans.get(key).add(s);
//        }
//        return new ArrayList(ans.values());
    }

}
