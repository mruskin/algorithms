package uber.arraysAndStrings;

import java.util.Arrays;

/**
 * https://leetcode.com/explore/interview/card/uber/289/array-and-string/1683/
 * Find First and Last Position of Element in Sorted Array
 * Given an array of integers nums sorted in ascending order, find the starting and ending position of a given target value.
 *
 * If target is not found in the array, return [-1, -1].
 *
 * You must write an algorithm with O(log n) runtime complexity.
 *
 *
 *
 * Example 1:
 *
 * Input: nums = [5,7,7,8,8,10], target = 8
 * Output: [3,4]
 */
public class FirstAndLastPositionOfElement {

    public static void main(String[] args) {
        System.out.println(Arrays.toString( searchRange(new int[]{5, 6, 8, 8}, 8)));
    }

    // Find left index
    // find middle element
    // create two potnetrs to top left and right
    // create a pointer for the leftEdge
    // if mid element is higher than target, then decrement right pointer
    // if mid element is less than target, then increment left pointer
    // else target is midElement, leftEdge = to the found index and decrement rightPointer
    // Find right index
    //

    public static int[] searchRange(int[] nums, int target) {
        if (nums.length == 0) {
            return new int[]{-1, -1};
        }
    // lets find left index at first
        int left = leftEdge(nums, target);

        if (left == -1) {
            return new int[]{-1, -1};
        }

        int right = findRightMostIndex(nums, target);

        return new int[]{left, right};

    }

    public static int leftEdge(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1; //3
        int leftMostIndex = -1;
        while (left <= right) {
            int mid = (left + right) / 2;
            System.out.println("mid: " + mid);
            if (nums[mid] > target) {
                right = mid - 1;
            } else if (nums[mid] < target) {
                left = mid + 1;
            } else { // in case if it is equal to target
                leftMostIndex = mid;
                right = mid - 1;
            }
            System.out.println(leftMostIndex);
        }
        return leftMostIndex;
    }

    public static int findRightMostIndex(int[] nums,int target) {
        int left = 0;
        int right = nums.length - 1;
        int rightMostIndex = -1;

        while (left <= right) {
            int mid = (left + right) / 2;
            if (nums[mid] > target) {
                right = mid - 1;
            } else if (nums[mid] < target) {
                left = mid + 1;
            } else { // in case if it is equal to target
                rightMostIndex = mid;
                left = mid + 1;
            }
        }
        return rightMostIndex;
    }



//    public int[] searchRange(int[] nums, int target) {
//
//        int firstOccurrence = this.findBound(nums, target, true);
//
//        if (firstOccurrence == -1) {
//            return new int[]{-1, -1};
//        }
//
//        int lastOccurrence = this.findBound(nums, target, false);
//
//        return new int[]{firstOccurrence, lastOccurrence};
//    }
//
//    private int findBound(int[] nums, int target, boolean isFirst) {
//        // array length
//        int N = nums.length;
//        // two pointers for the start and the end
//        int begin = 0, end = N - 1;
//
//        while (begin <= end) {
//
//            int mid = (begin + end) / 2;
//
//            if (nums[mid] == target) {
//
//                if (isFirst) {
//
//                    // This means we found our lower bound.
//                    // If mid is the last element or the next element to the left is not target
//                    if (mid == begin || nums[mid - 1] != target) {
//                        return mid;
//                    }
//
//                    // Search on the left side for the bound.
//                    end = mid - 1;
//
//                } else {
//
//                    // This means we found our upper bound.
//                    if (mid == end || nums[mid + 1] != target) {
//                        return mid;
//                    }
//
//                    // Search on the right side for the bound.
//                    begin = mid + 1;
//                }
//
//            } else if (nums[mid] > target) {
//                end = mid - 1;
//            } else {
//                begin = mid + 1;
//            }
//        }
//
//        return -1;
//    }

//    public static int[] searchRange2(int[] nums, int target) {
//        int counter = 0;
//        int max = 0;
////        int[] res = new int[];
//        Map<Integer, Integer> map = new HashMap<>();
//        for (int i = 0; i < nums.length; i++ ) {
//            if (nums[i] == target) {
//                map.put(target, i);
//                counter++;
//                max = counter;
//            } else {
//                counter = 0;
//            }
//        }
//        return new int[] {max};
//    }

}
