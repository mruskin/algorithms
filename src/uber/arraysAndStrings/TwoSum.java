package uber.arraysAndStrings;

import com.sun.jdi.Value;

import java.util.*;

/**
 * https://leetcode.com/problems/two-sum/solution/
 *
 * Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
 *
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 *
 * You can return the answer in any order.
 * Input: nums = [2,7,11,15], target = 9
 * Output: [0,1]
 * Output: Because nums[0] + nums[1] == 9, we return [0, 1].
 */
public class TwoSum {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(twoSum(new int[] {2,7,11,15}, 9)));
    }

    public static int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        Map<Integer,Integer> set = new HashMap();
        for (int i =0; i < nums.length; i++) {
            if (set.containsKey(nums[i])) {
                result[0] = set.get(nums[i]);
                result[1] = i;
            }
            set.put(Integer.valueOf(target - nums[i]), i);
        }
        return result;


        // BETTER SOLUTION
//        Map<Integer, Integer> map = new HashMap<>();
//        for (int i = 0; i < nums.length; i++) {
//            int complement = target - nums[i];
//            if (map.containsKey(complement)) {
//                return new int[] { map.get(complement), i };
//            }
//            map.put(nums[i], i);
//        }
//        throw new IllegalArgumentException("No two sum solution");
    }


}
