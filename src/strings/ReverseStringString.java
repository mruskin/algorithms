package strings;

public class ReverseStringString {

    public static void main(String[] args) {
        System.out.println(reverseString("Hi my name is miha"));
    }

    private static String reverseString(String reverse) {
        // check input
        if (reverse == null || reverse.length() == 0) {
            return "hmm wrong input";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = reverse.length()-1; i >= 0; i--) {
            sb.append(reverse.charAt(i));
        }
        return sb.toString();
    }
}
