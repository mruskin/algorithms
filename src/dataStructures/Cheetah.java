package dataStructures;

public class Cheetah extends  Speedster{


    int numSports;

    public Cheetah(int numSports) {
        super.numSports = numSports;
    }

    public static void main(String[] args) {
        Speedster s = new Cheetah(50);
        System.out.println(s.numSports);
    }
}
