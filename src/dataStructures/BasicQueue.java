package dataStructures;

import java.util.Arrays;

/**
 * Queue is FIFO
 * @param <X>
 */
public class BasicQueue<X> {

    private X[] data;
    private int front;
    private int end;

    public BasicQueue() {
        this(1000);
    }

    public BasicQueue(int size) {
        this.front = -1;
        this.end = -1;
        data = (X[]) new Object[size];
    }

    public void enQueue(X item) {
        if (size() == 0) {
            front = front + 1;
        }
        end = end + 1;
        data[end] = item;
    }

    public X deQueue() {
        X temp = null;
        if (size() == 0) {
            throw new IllegalArgumentException("Noting in queue");
        }
        if (front == end) {
            temp = data[front];
            data[front] = null;
            front = -1;
            end = -1;
        } else {
            temp = data[front];
            data[front] = null;
            front = front + 1;
        }

        return temp;
    }

    public int size() {
        if (front == -1 && end == -1) {
            return 0;
        } else {
            return end - front + 1;
        }
    }

    public boolean contains(X item) {
        boolean contains = false;
        if (size() == 0 || item == null) {
            return false;
        }
        for (int i = front; i < end; i++) {
            if (data[i].equals(item)) {
                contains = true;
                break;
            }
        }
        return contains;

    }

    public X access(int position) {
        if (size() == 0 || position > end || position < front) {
            throw new IllegalArgumentException("Wrong input");
        }
        int trueIndex = 0;
        for (int i = front; i < end; i++) {
            if (trueIndex == position) {
                return data[i];
            }
            trueIndex++;
        }
        throw new IllegalArgumentException("Nothing found");

    }
}
