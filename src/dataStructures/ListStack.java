package dataStructures;

import java.util.ArrayList;

public class ListStack<X> implements Stack<X> {

    private ArrayList<X> data;
    private int stackPointer;

    public ListStack() {
        data = new ArrayList<>();
        stackPointer = 0;
    }

    @Override
    public void push(X newItem) {
        data.add(newItem);
        stackPointer=data.size();
        System.out.println(data);
        System.out.println(stackPointer);
    }

    @Override
    public X pop() {
        X tempObj = data.get(stackPointer - 1);
        data.remove(stackPointer - 1);
        stackPointer = data.size();
        return tempObj;
    }

    @Override
    public boolean contains(X item) {
        return data.contains(item);
    }

    @Override
    public X access(X item) {
        while (stackPointer > 0) {
            X tmpItem = pop();
            if (tmpItem.equals(item)) {
               return  tmpItem;
            }
        }
        throw new IllegalArgumentException("No data found");
    }

    @Override
    public int size() {
        return stackPointer;
    }
}
