package dataStructures;

/**
 * Stack is LIFO
 * @param <X>
 */
public class BasicStack<X> implements Stack<X>  {

    private X[] data;
    private int stackPointer;

    public BasicStack() {
        data = (X[]) new Object[1000];
        stackPointer = 0;
    }

    @Override
    public void push(X newItem) {
        data[stackPointer++] = newItem;
    }

    @Override
    public X pop() {
        if (stackPointer == 0) {
            throw new IllegalArgumentException("no more arguments in a stack");
        }
        return data[--stackPointer];
    }

    @Override
    public boolean contains(X item) {
        for (int i = 0; i < stackPointer; i++) {
            if (data[i].equals(item)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public X access(X item) {
        while (stackPointer > 0) {
            X tmpItem = pop();
            if (tmpItem.equals(item)) {
                return tmpItem;
            }
        }
        throw new IllegalArgumentException("No data found");
    }

    @Override
    public int size() {
        return stackPointer;
    }

}
