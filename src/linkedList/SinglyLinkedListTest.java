package linkedList;

public class SinglyLinkedListTest {

    public static void main(String[] args) {
//        Integer i = 1;
//        SinglyLinkedList singlyLinkedList = new SinglyLinkedList();
//        singlyLinkedList.insertAtHead(i);
//        singlyLinkedList.insertAtHead(i+2);
//        System.out.println(singlyLinkedList.isEmpty());
//        singlyLinkedList.printList();

            SinglyLinkedList<Integer> sll = new SinglyLinkedList<>();
            for (int i = 1; i <= 6; i++) {
                sll.insertAtEnd(i); // inserting value at the tail of the list
            }
            sll.printList();
            sll.insertAtHead(7);
//            sll.insertAfter(8, 2);
            sll.printList();
        System.out.println(sll.searchNode(1));
        sll.deleteAtHead();

            sll.printList();   // calling insert after
//            System.out.println ("\nInserting 10 after 3");
//            sll.insertAfter (10, 3);   // calling insert after
//            sll.printList();
    }
}
