package stack;

import java.lang.reflect.Array;
import java.util.Arrays;

public class TestTwoStacks {

    public static void main(String[] args) {
        TwoStacks<Integer> stack = new TwoStacks<Integer>(10);


        stack.push1(1);
        stack.push1(2);
        stack.push2(3);
        stack.push2(4);
        stack.push1(3);
        stack.push2(5);
        stack.push2(6);
        stack.pop2();
        stack.pop2();
        stack.push2(6);
        stack.push2(6);
        stack.push2(6);
        stack.pop1();

    }
}
