package stack.sortValuesInStack;

import java.util.Arrays;
import java.util.List;

/** EDUCATIVE  LIFO
 * Problem Statement #
 * In this problem, you have to implement the void sortStack(Stack<Integer> stack) method, which will take an Integer type stack and sort all its elements in ascending order. An illustration is also provided for your understanding.
 *
 * Method Prototype #
 * void sortStack(Stack<Integer> stack)
 * Output #
 * It returns an Integer type Stack with all its elements sorted in ascending order
 *
 * Sample Input #
 * stack = {23,60,12,42,4,97,2}
 * Sample Output #
 * result = {2,4,12,23,42,60,97}
 */
public class main {

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<Integer>(6);
        stack.push(23);
        stack.push(60);
        stack.push(12);
        stack.push(42);
        stack.push(4);
        stack.push(2);
        System.out.println(Arrays.toString(stack.getArr()));

    }

    public static void sortStack(Stack<Integer> stack) {
        //1. Use a second tempStack.
        //2. Pop value from mainStack.
        //3. If the value is greater or equal to the top of tempStack, then push the value in tempStack
        //else pop all values from tempStack and push them in mainStack and in the end push value in tempStack and repeat from step 2.
        //till mainStack is not empty.
        //4. When mainStack will be empty, tempStack will have sorted values in descending order.
        //5. Now transfer values from tempStack to mainStack to make values sorted in ascending order.
        Stack<Integer> newStack = new Stack<>(stack.getMaxSize());
        while (!stack.isEmpty()) {
            Integer value = stack.pop();
            if (!newStack.isEmpty() && value >= newStack.top()) {
                newStack.push(value);
            } else {
                while (!newStack.isEmpty() && newStack.top() > value)
                    stack.push(newStack.pop());
                newStack.push(value);
            }
        }
        while (!newStack.isEmpty())
            stack.push(newStack.pop());

    }

    /**
     * This solution takes an iterative approach towards the problem. We create a helper stack call newStack. Its job is to hold the elements of the stack in descending order.
     *
     * The main functionality of the algorithm lies in the nested while loops. In the outer loop, we pop elements out of stack until it is empty. As long as the popped value is larger than the top value in tempStack, we can push it in.
     *
     * The inner loop begins when stack.pop() gives us a value that is smaller than the top of newStack. All the elements (they are sorted) of newStack are shifted back to stack, and the value is pushed into newStack. This ensures that the bottom of newStack always holds the smallest value from the stack.
     *
     * When stack becomes empty, all the elements are in newStack in descending order. Now we simply push them back into the stack, resulting in the whole stack being sorted in ascending fashion.
     */

}
