package miro;

import java.util.Arrays;

/**
 *  LEETCODE solution cake example
 *  https://leetcode.com/problems/maximum-area-of-a-piece-of-cake-after-horizontal-and-vertical-cuts/solution/
 */
public class MaxPeaceOfCakeArea {

    public static void main(String[] args) {
        System.out.println(sliceCake(2, 2, new int[]{1}, new int[]{2}));
    }
    /**
     *
     * @param h - horizontal bar
     * @param w - vertical bar
     * @param horizontalCuts - removed bar horizontal
     * @param verticalCuts - removed bar, vertical
     * @return
     */
    public static long sliceCake(int h, int w, int[] horizontalCuts, int[] verticalCuts) {
        // Start by sorting the inputs
        Arrays.sort(horizontalCuts);
        Arrays.sort(verticalCuts);
        int n = horizontalCuts.length;
        int m = verticalCuts.length;

        // Consider the edges first
        System.out.println("n = " + n);
        System.out.println("horizontalCuts[0] " + horizontalCuts[0]);
        System.out.println("horizontalCuts[n - 1]  " + horizontalCuts[n - 1]);

        // let's consider the edges from the beginning. Narisuj risunok.
        // nado vychislit naibolshuju vysotu. I udobnej nachat s krajov
        long maxHeight = Math.max(horizontalCuts[0], h - horizontalCuts[n - 1]);
        System.out.println("max height: " + maxHeight);
        for (int i = 1; i < n; i++) {
            // horizontalCuts[i] - horizontalCuts[i - 1] represents the distance between
            // two adjacent edges, and thus a possible height
            // nado najti naibolshij promezhutok mezhdu gorizontalnymi linijami
            // sravnivaem predydushij sortirovannyj element massiva so sledujushim
            maxHeight = Math.max(maxHeight, horizontalCuts[i] - horizontalCuts[i - 1]);
            System.out.println("loop: " + maxHeight);
        }

        // Consider the edges first
        long maxWidth = Math.max(verticalCuts[0], w - verticalCuts[m - 1]);
        System.out.println("max width: " + maxWidth);
        for (int i = 1; i < m; i++){
            // verticalCuts[i] - verticalCuts[i - 1] represents the distance between
            // two adjacent edges, and thus a possible width
            maxWidth = Math.max(maxWidth, verticalCuts[i] - verticalCuts[i - 1]);
            System.out.println("loop2: " + maxHeight);
        }

        // Be careful of overflow, and don't forget the modulo!
        return (int) ((maxWidth * maxHeight) % (1e9 + 7));

    }

}
