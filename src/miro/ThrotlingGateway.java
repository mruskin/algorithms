package miro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * https://leetcode.com/discuss/interview-question/819577/Throttling-Gateway-Hackerrank
 */
public class ThrotlingGateway {

    public static void main(String[] args) {
        List<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(1);
        test.add(1);
        test.add(1);
        test.add(2);
//        test.add(2);
//        test.add(2);
//        test.add(3);
//        test.add(3);
//        test.add(3);
//        test.add(4);
//        test.add(4);
//        test.add(4);
//        test.add(5);
//        test.add(5);
//        test.add(5);
//        test.add(6);
//        test.add(6);
//        test.add(6);
//        test.add(7);
//        test.add(7);
        System.out.println(droppedRequests(test));
    }

    public static int droppedRequests(List<Integer> requestTime) {

//        int[] requestTimeArr = requestTime.stream().mapToInt(i -> i).toArray();

        int dropped = 0;
//        System.out.println(Arrays.toString(requestTimeArr));

        for (int i = 0; i<  requestTime.size(); i++) {
//        for (int i = 0; i < requestTimeArr.length; i++) {
            if (i > 2 && requestTime.get(i) == requestTime.get(i - 3)) {
                ++dropped;
            } else if (i > 19 && (requestTime.get(i)  - requestTime.get(i-20)) < 10) {
                ++dropped;
            } else if (i > 59 && (requestTime.get(i)  - requestTime.get(i-60)) < 60) {
                ++dropped;
            }

//                if (i > 2 && requestTimeArr[i] == requestTimeArr[i - 3]) {
//                    ++dropped;
//                } else if (i > 19 && (requestTimeArr[i] - requestTimeArr[i - 20]) < 10) {
//                    ++dropped;
//                } else if (i > 59 && (requestTimeArr[i] - requestTimeArr[i - 60]) < 60) {
//                    ++dropped;
//                }
            }

            return dropped;
        }


    }
