package miro;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Pairs of Songs With Total Durations Divisible by 60
 * https://leetcode.com/problems/pairs-of-songs-with-total-durations-divisible-by-60/
 *
 * You are given a list of songs where the ith song has a duration of time[i] seconds.
 *
 * Return the number of pairs of songs for which their total duration in seconds is divisible by 60. Formally, we want the number of indices i, j such that i < j with (time[i] + time[j]) % 60 == 0.
 *
 *
 *
 * Example 1:
 *
 * Input: time = [30,20,150,100,40]
 * Output: 3
 * Explanation: Three pairs have a total duration divisible by 60:
 * (time[0] = 30, time[2] = 150): total duration 180
 * (time[1] = 20, time[3] = 100): total duration 120
 * (time[1] = 20, time[4] = 40): total duration 60
 */
public class PairsOfSongs {
    // the easiest solution is to do a bruteforce
    // and create a nested loop
    // and iterate over the array with two pointers
//    int count = 0;
//        for (int i = 0; i < time.length; i++) {
//        for (int j = i+1; j < time.length; j++) {
//            System.out.println(time[i] + " :: " +  time[j]);
//            if ((time[i] + time[j]) % 60 == 0) {
//                count++;
//            }
//        }
//    }
//        return count;

    public static void main(String[] args) {

        System.out.println(numPairsDivisibleBy60(new int[]{20   , 20}));
    }

    // another solution should be something with the numbers
    // we need to figure out what is a common denominator
    // we can have a counter
    // when number is devided by 60 we can increment it
    // we need a logic that will hold both the remainder and the current element

    public static int numPairsDivisibleBy60(int[] time) {

        int count = 0;
        Map<Integer, Integer> map = new HashMap<>();

        for (int t: time) { // TODO you can write loop at first

            int remainder = t % 60; // the reminder can be from 0-60

            int pairedKey = remainder == 0 ? remainder : 60 - remainder; // either 0 if devided by 60, or

            count += map.getOrDefault(pairedKey, 0);

            map.put(remainder, map.getOrDefault(remainder, 0) + 1);
        }

        return count;
    }

}
