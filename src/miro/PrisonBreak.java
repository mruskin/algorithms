package miro;

import java.util.Arrays;
import java.util.List;

/**
 * https://github.com/kaushal02/interview-coding-problems/blob/master/prisonBreak.cpp
 * https://www.chegg.com/homework-help/questions-and-answers/5-prison-break-prisoner-planning-escape-prisonl-prison-s-gate-consists-horizontal-vertical-q55473444
 * LEETCODE solution cake example
 * https://leetcode.com/problems/maximum-area-of-a-piece-of-cake-after-horizontal-and-vertical-cuts/solution/

 */
public class PrisonBreak {


    public static void main(String[] args) {
        System.out.println(prison(6, 6, new int[]{4}, new int[]{2}));
    }

    /**
     *
     * @param h - horizontal bar
     * @param w - vertical bar
     * @param horizontalCuts - removed bar horizontal
     * @param verticalCuts - removed bar, vertical
     * @return
     */
    public static long prison(int h, int w, int[] horizontalCuts, int[] verticalCuts) {
        // Start by sorting the inputs
        Arrays.sort(horizontalCuts);
        Arrays.sort(verticalCuts);
        h++;
        w++;

        // let's consider the edges from the beginning. Narisuj risunok.
        // nado najti naibolshuju dyrku vo gorizontali

        int[] barsWeHave = new int[h - horizontalCuts.length];
        // let's count the amount of bars we have
        barsWeHave(h, horizontalCuts, barsWeHave);

        long maxHeight = Math.max(barsWeHave[0], h - barsWeHave[barsWeHave.length - 1]);
        System.out.println("max height: " + maxHeight);
        System.out.println(Arrays.toString(barsWeHave));
        for (int i = 0; i < barsWeHave.length -1; i++) {
            maxHeight = Math.max(maxHeight, barsWeHave[i+1] - barsWeHave[i]);
            System.out.println("loop: " + maxHeight);
        }

        int[] widthWeHave = new int[h - verticalCuts.length];
        barsWeHave(w, verticalCuts, widthWeHave);

        long maxWidth = Math.max(widthWeHave[0], w - widthWeHave[widthWeHave.length - 1]);
        System.out.println("max width: " + maxWidth);
        System.out.println(Arrays.toString(widthWeHave));
        for (int i = 0; i < widthWeHave.length -1; i++) {
            maxWidth = Math.max(maxWidth, widthWeHave[i+1] - widthWeHave[i]);
            System.out.println("loop2: " + maxWidth);
        }

        // Be careful of overflow, and don't forget the modulo!
        return (int) (maxHeight*maxWidth);

    }

    private static void barsWeHave(int h, int[] horizontalCuts, int[] barsWeHave) {
        int counter = 0;
        int counter2 = 0;
        for (int i = 1; i <= h; i++) {
            if (counter < horizontalCuts.length && horizontalCuts[counter] == i) {
                counter++;
            } else {
                barsWeHave[counter2++] = i;
            }
        }
    }

}
