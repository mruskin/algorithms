package miro;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * https://github.com/nataliekung/leetcode/blob/master/twitter-oa/parking-dilemma.md
 *
 * Example:
 * Input:
 * cars: [2, 10, 8, 17]
 * k: 3
 * output: 9
 * Explanation: you can build a roof of length 9 covering all parking slots
 * from the 2nd one to the 10th one, so covering 3 cars at slots 2, 10, 8,
 * there are no shorter roof  that can cover 3 cars, so the answer is 9
 */
public class ParkingDilemma {
// 2, 8 , 10, 17
    public static void main(String[] args) {
        System.out.println(findPlace(new ArrayList<>(List.of(2, 10, 8, 17, 18, 19)), 3));
    }

    public static int findPlace(List<Integer> cars, int k) {
        Collections.sort(cars);
        int leftpointer = 0, rightpointer = 0, min = Integer.MAX_VALUE;
        while (leftpointer < cars.size()) {
            rightpointer = rightpointer + (k-1);
            if (rightpointer > cars.size()-1) {
                leftpointer++;
                rightpointer=leftpointer;
                continue;
            }
            int temp = cars.get(rightpointer) - cars.get(leftpointer) +1;
            System.out.println(cars.get(rightpointer) + " - "  + cars.get(leftpointer));
            if (temp < min) {
                min = temp;
            }
            
        }
        return min;
    }
//another solution
    /**
     *  public static int minRoofLength(int[] arr, int k){
     *     Arrays.sort(arr);
     *     int start = 0;
     *     int minRoofLength = Integer.MAX_VALUE;
     *     for(int end=0;end<arr.length;end++){
     *       if(end < k-1) continue;
     *       int currentRoofLength = arr[end]-arr[start++]+1;
     *       minRoofLength = Math.min(minRoofLength, currentRoofLength);
     *     }
     *     return minRoofLength;
     *   }
     */
}
