package queue;

import java.util.Random;

/** EDUCATIVE   FIFO
 * In this problem, using a queue, you have to implement the String[] findBin(int number) method to generate binary numbers from 1 to any given number (n). An illustration is also provided for your understanding.
 *
 * Method Prototype #
 * String[] findBin(int number)
 * Input #
 * A positive integer n.
 *
 * Output #
 * It returns binary numbers up to n.
 *
 * Sample Input #
 * n = 3
 * Sample Output #
 * result = {"1","10","11"}
 */
public class GenerateBinNumbers {

    //1.Start with Enqueuing 1.
    //2.Dequeue a number from queue and append 0 to it and enqueue it back to queue.
    //3.Perform step 2 but with appending 1 to the original number and enqueue back to queue.
    //Size of Queue should be 1 more than number because for a single number we're enqueuing two
    public static String[] findBin(int number) {
        String[] result = new String[number];
        Queue<String> queue = new Queue<>(number + 1);

        queue.enqueue("1");

        for (int i = 0; i < number; i++) { // i = 0  1
            result[i] = queue.dequeue(); // res = 1  10
            System.out.println(result[i]);
            String s1 = result[i] + "0"; // 10 100
            String s2 = result[i] + "1"; // 11 101
            queue.enqueue(s1); // 10
            queue.enqueue(s2); //11
            System.out.println(s1);
            System.out.println(s2);
        }

        return result; //For number = 3 , result = {"1","10","11"};
        }


    public static void main(String args[]) {

        String[] output = findBin(8);
        for(int i = 0; i < 8; i++)
            System.out.print(output[i] + " ");
    }

}
