package queue.reverseFirstElementsOfTheQueue;

/** EDUCATIVE
 * Problem Statement #
 * In this problem, you have to implement the void reverseK(Queue<V> queue, int k) method; this will take a queue and any number (k) as input, and reverse the first k elements of the queue. An illustration is also provided for your understanding.
 *
 * Method Prototype #
 * void reverseK(Queue<V> queue, int k)
 * Output #
 * An array with the first “k” elements reversed.
 *
 * Sample Input #
 * Queue = {1,2,3,4,5,6,7,8,9,10}
 * k = 5
 * Sample Output #
 * result = {5,4,3,2,1,6,7,8,9,10}
 */
public class Exercise {

    //1.Push first k elements in queue in a stack.
    //2.Pop Stack elements and enqueue them at the end of queue
    //3.Dequeue queue elements till "k" and append them at the end of queue
    //4.Dequeue the remaining elements and enqueue them again to append them at end of the queue
    public static <V> void reverseK(Queue<V> queue, int k) {
        if (queue.isEmpty() || k <= 0)
            return;
        Stack<V> stack = new Stack<>(k);

        while(!stack.isFull())
            stack.push(queue.dequeue());

        while(!stack.isEmpty())
            queue.enqueue(stack.pop());

        int size = queue.getCurrentSize();
        for(int i = 0; i < size - k; i++)
            queue.enqueue(queue.dequeue());
    }
    public static void main(String args[]) {

        Queue<Integer> queue = new Queue<Integer>(10);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(6);
        queue.enqueue(7);
        queue.enqueue(8);
        queue.enqueue(9);
        queue.enqueue(10);

        reverseK(queue,5);

        System.out.print("Queue: ");
        while(!queue.isEmpty()) {
            System.out.print(queue.dequeue() + " ");
        }
    }

}

/**
 * Explanation #
 * reverseK(queue, k) takes queue as an input parameter, where k represents the number of elements we want to reverse.
 *
 * Now, moving on to the actual logic of the method. First, check if the queue is empty or if k is a valid non-zero positive number. If either of them is false, then do nothing, just return.
 *
 * If none of the conditions given above are true, then create a Stack. Start removing first k values from the queue and push them to the stack by using stack.push(queue.dequeue).
 *
 * Once all the k values have been pushed to the stack, start popping them and enqueueing them to the back of the queue sequentially. We will do this using queue.enqueue(stack.pop()). At the end of this step, we will be left with an empty stack, and the k reversed elements will be appended to the back of the queue.
 *
 * Now we need to move these reversed elements to the front of the queue. To do this, we used queue.enqueue(queue.dequeue()). Each element is first dequeued from the back.
 *
 * Time Complexity #
 * Overall, kk elements are dequeued, pushed to the stack, popped from it, and then enqueued. Additionally, n-kn−k elements are dequeued and enqueued to the queue. Each push, pop, enqueue, or dequeue operation takes constant time; the time complexity of this method is O(n)O(n) as all nn elements have to be processed with constant-time​ operations.
 */
